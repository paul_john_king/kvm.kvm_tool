<!--
The script `readme.sh` in the project's root generates this document from the
project's sources.  In order to change this document, edit the sources and call
`readme.sh`
-->

`kvm_tool` – a Bourne shell script to easily create and delete libvirt
storage volumes and domains.

KVM Tool
========

Usage
-----

    kvm_tool create volume [-x] «volume» «pool» «path»
    kvm_tool delete volume «volume» «pool»

    kvm_tool create domain [-b «bridge₁»] … [-b «bridgeₗ»] [-n «network₁»] … [-n «networkₘ»] [-m «path₁»] … [-m «pathₙ»] [-r|-p] [-x] «domain» «cpu_count» «ram_count»[«ram_unit»] «volume» «pool»
    kvm_tool ensure running «domain»
    kvm_tool ensure paused «domain»
    kvm_tool ensure shut off [-t «seconds»] «domain»
    kvm_tool delete domain [-t «seconds»] «domain»

    kvm_tool usage
    kvm_tool help

`kvm_tool` requires the command `virsh`.

Create Volume
-------------

    kvm_tool create volume [-x] «volume» «pool» «path»

Try to create a libvirt storage volume `«volume»` in the libvirt storage pool
`«pool»` with the qcow2 image file at the path `«path»` as its backing
store.

*   `-x` – write to the standard output the libvirt XML that would create the
    storage volume, but do not create it.

Delete Volume
-------------

    kvm_tool delete volume «volume» «pool»

Try to delete the libvirt storage volume `«volume»` from the libvirt storage
pool `«pool»`.

Create Domain
-------------

    kvm_tool create domain [-b «bridge₁»] … [-b «bridgeₗ»] [-n «network₁»] … [-n «networkₘ»] [-m «path₁»] … [-m «pathₙ»] [-r|-p] [-x] «domain» «cpu_count» «ram_count»[«ram_unit»] «volume» «pool»

Try to create a libvirt domain `«domain»` with `«cpu_count»` virtual CPUs,
`«ram_count»[«ram_unit»]` RAM, and the libvirt storage volume `«volume»` in
the libvirt storage pool `«pool»` as its only disk device, where

*   the obligatory `«cpu_count»` and `«ram_count»` are non-zero natural
    numbers without leading zeroes, and

*   the optional `«ram_unit»` is any of

    *   `b` or `bytes` for bytes,

    *   `KB` for kilobytes (10³ bytes),

    *   `MB` for megabytes (10⁶ bytes),

    *   `GB` for gigabytes (10⁹ bytes),

    *   `TB` for terabytes (10¹² bytes),

    *   `k` or `KiB` for kibibytes (2¹⁰ bytes),

    *   `M` or `MiB` for mebibytes (2²⁰ bytes),

    *   `G` or `GiB` for gibibytes (2³⁰ bytes) and

    *   `T` or `TiB` for tebibytes (2⁴⁰ bytes).

    It defaults to the libvirt default (currently `KiB`) if not given.

The domain is in the state `shut off` and autostart is not enabled.

*   `-b «bridgeᵢ»` – Create a network device on the domain connected to the
    host bridge `«bridgeᵢ»`.

*   `-n «networkⱼ»` – Create a network device on the domain connected to the
    libvirt network `«networkⱼ»`.

*   `-m «pathₖ»` – Make the host directory at the path `«pathₖ»` available
    to the domain as a `virtiofs` file system with the mount tag
    `mount«k»`.  In the domain, call

        mount -t virtiofs mount«k» «path»

    or add the line

        mount«k» «path» virtiofs defaults 0 0

    to the file `/etc/fstab` and call

        mount -a

    as the user `root` to mount the file system at the path `«path»` in the
    domain.

*   `-r` – Put the newly-created domain in the state `running`.

*   `-p` – Put the newly-created domain in the state `paused`.

*   `-x` – write to the standard output the libvirt XML that would create the
    domain, but do not create it.

Ensure Running
--------------

    kvm_tool ensure running «domain»

Try to ensure the libvirt domain `«domain»` is in the state `running`.

Ensure Paused
-------------

    kvm_tool ensure paused «domain»

Try to ensure the libvirt domain `«domain»` is in the state `paused`.

Ensure Shut Off
---------------

    kvm_tool ensure shut off [-t «seconds»] «domain»

Try to ensure the libvirt domain `«domain»` is in the state `shut off`.

*   `-t «seconds»` – Wait up to `«seconds»` seconds for the domain to shut
    off before forcing it.  Do not wait if not given.

*   `-t «seconds»` – Wait up to `«seconds»` seconds for the domain to
    resume (if paused) and shut down (if running) before forcing it into the
    state `shut off`.  Do not wait if not given.

Delete Domain
-------------

    kvm_tool delete domain [-t «seconds»] «domain»

Try to delete the domain `«domain»`.

*   `-t «seconds»` – Wait up to `«seconds»` seconds for the domain to
    resume (if paused) and shut down (if running) before forcing it into the
    state `shut off` and deleting it.  Do not wait if not given.

Warning
=======

The script `readme.sh` in the project's root generates this document from the
project's sources.  In order to change this document, edit the sources and call
`readme.sh`
