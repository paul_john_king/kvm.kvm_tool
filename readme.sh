#!/bin/sh -eu

_echo_stdin(){
	local _line;

	while IFS="" read -r line;
	do
		printf "%s\n" "${line}";
	done;

	return 0;
};

_main(){
	local _SELF="${0##*/}";
	local _HOME="${0%/*}";

	local _TOOL;
	local _README;
	local _WARNING;

	_TOOL="kvm_tool";
	_README="${_HOME}/README.md";
	_WARNING=$(
		_echo_stdin << \
───────────────────────────────────────────────────────────────────────────────
The script \`${_SELF}\` in the project's root generates this document from the
project's sources.  In order to change this document, edit the sources and call
\`${_SELF}\`
───────────────────────────────────────────────────────────────────────────────
	);

	_echo_stdin > "${_README}" << \
───────────────────────────────────────────────────────────────────────────────
<!--
${_WARNING}
-->

\`${_TOOL}\` – a Bourne shell script to easily create and delete libvirt
storage volumes and domains.

KVM Tool
========

$("${_HOME}/${_TOOL}" help)

Warning
=======

${_WARNING}
───────────────────────────────────────────────────────────────────────────────

	return 0;
};

_main "${@}";
